package io.swagger.codegen;

import io.swagger.codegen.v3.*;
import io.swagger.codegen.v3.CodegenType;

import io.swagger.codegen.v3.generators.DefaultCodegenConfig;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.codegen.v3.CodegenOperation;

import java.util.*;
import java.io.File;

public class CucumberTestsGenerator extends DefaultCodegenConfig {

  // source folder where to write the files
  protected String sourceFolder = "features";
  protected String apiVersion = "1.0.0";

  /**
   * Configures the type of generator.
   * 
   * @return  the CodegenType for this generator
   * @see     io.swagger.codegen.CodegenType
   */
  public CodegenType getTag() {
    return CodegenType.OTHER;
  }

  /**
   * Configures a friendly name for the generator.  This will be used by the generator
   * to select the library with the -l flag.
   * 
   * @return the friendly name for the generator
   */
  public String getName() {
    return "cucumber-tests";
  }

  /**
   * Returns human-friendly help for the generator.  Provide the consumer with help
   * tips, parameters here
   * 
   * @return A string value for the help message
   */
  public String getHelp() {
    return "Generates a cucumber-tests client library.";
  }

  public CucumberTestsGenerator() {
    super();

    // set the output folder here
    outputFolder = "generated-code/cucumber-tests";

    /**
     * Models.  You can write model files using the modelTemplateFiles map.
     * if you want to create one template for file, you can do so here.
     * for multiple files for model, just put another entry in the `modelTemplateFiles` with
     * a different extension
     */
    modelTemplateFiles.put(
     "schema.mustache", // the template to use
     ".json");       // the extension for each file to write

    /**
     * Api classes.  You can write classes for each Api file with the apiTemplateFiles map.
     * as with models, add multiple entries with different extensions for multiple files per
     * class
     */
    apiTemplateFiles.put(
      "api.mustache",   // the template to use
      ".feature");       // the extension for each file to write

    /**
     * Template Location.  This is the location which templates will be read from.  The generator
     * will use the resource stream to attempt to read the templates.
     */
    templateDir = "cucumber-tests";

    /**
     * Api Package.  Optional, if needed, this can be used in templates
     */
    apiPackage = "io.swagger.client.api";

    /**
     * Model Package.  Optional, if needed, this can be used in templates
     */
    modelPackage = "io.swagger.client.model";

    /**
     * Reserved words.  Override this with reserved words specific to your language
     */
    reservedWords = new HashSet<String> (
      Arrays.asList(
        "sample1",  // replace with static values
        "sample2")
    );

    /**
     * Additional Properties.  These values can be passed to the templates and
     * are available in models, apis, and supporting files
     */
    additionalProperties.put("apiVersion", apiVersion);

    /**
     * Supporting Files.  You can write single files for the generator with the
     * entire object tree available.  If the input file has a suffix of `.mustache
     * it will be processed by the template engine.  Otherwise, it will be copied
     */
    supportingFiles.add(new io.swagger.codegen.v3.SupportingFile("Gemfile",   // the input template or file
      "",                                                       // the destination folder, relative `outputFolder`
      "Gemfile")                                          // the output file
    );

    supportingFiles.add(new io.swagger.codegen.v3.SupportingFile("Dockerfile",   // the input template or file
      "",                                                       // the destination folder, relative `outputFolder`
      "Dockerfile")                                          // the output file
    );

    supportingFiles.add(new io.swagger.codegen.v3.SupportingFile("entrypoint.sh",   // the input template or file
      "",                                                       // the destination folder, relative `outputFolder`
      "entrypoint.sh")                                          // the output file
    );

    supportingFiles.add(new io.swagger.codegen.v3.SupportingFile("env.rb",   // the input template or file
      sourceFolder + "/support/",                                                       // the destination folder, relative `outputFolder`
      "env.rb")                                          // the output file
    );

    /**
     * Language Specific Primitives.  These types will not trigger imports by
     * the client generator
     */
    languageSpecificPrimitives = new HashSet<String>(
      Arrays.asList(
        "Type1",      // replace these with your types
        "Type2")
    );
  }

  /**
   * Escapes a reserved word as defined in the `reservedWords` array. Handle escaping
   * those terms here.  This logic is only called if a variable matches the reserved words
   * 
   * @return the escaped term
   */
  @Override
  public String escapeReservedWord(String name) {
    return "_" + name;  // add an underscore to the name
  }

  /**
   * Location to write model files.  You can use the modelPackage() as defined when the class is
   * instantiated
   */
  public String modelFileFolder() {
    return outputFolder + "/" + sourceFolder + "/" + "schemas";
  }

  /**
   * Location to write api files.  You can use the apiPackage() as defined when the class is
   * instantiated
   */
  @Override
  public String apiFileFolder() {
    return outputFolder + "/" + sourceFolder;
  }

  @Override
  public String getArgumentsLocation() {
    return null;
  }

  @Override
  protected String getTemplateDir() {
    return templateDir;
  }

  @Override
  public String getDefaultTemplateDir() {
    return templateDir;
  }

  @Override
  public Map<String, Object> postProcessOperations(Map<String, Object> objs) {
    if(objs.get("operations") != null){
      HashMap<String, Object> opers = (HashMap<String, Object>) objs.get("operations");
      
      for(String k :opers.keySet()){
        if(k == "operation"){
          for(CodegenOperation o : (List<CodegenOperation>) opers.get(k)){
            o.getVendorExtensions().put("isGet", o.httpMethod.toUpperCase().equals("GET"));
            o.getVendorExtensions().put("isPost", o.httpMethod.toUpperCase().equals("POST"));
            o.getVendorExtensions().put("isPut", o.httpMethod.toUpperCase().equals("PUT"));
            o.getVendorExtensions().put("isDelete", o.httpMethod.toUpperCase().equals("DELETE"));
          }
        }
      }


    }
    return objs;
  }
}